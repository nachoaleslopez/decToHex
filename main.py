import sys
from lib import decimalToHexadecimal

def main():
    arguments = sys.argv
    # First, we'll parse each argument given
    nextArgumentOfOption = False
    if len(arguments) <= 1:
        # We exit the program and print a help page
        print("""decToHex: A decimal to hexadecimal converter
In order to use this program, you must use one of the following options:
 -f - This will read the file path introduced and attempt to print the converted value of each line
 -n - This will read the following argument of this option and convert it. The last argument will also use this option by default""")
        exit()
    for (index, value) in enumerate(arguments):
        if not nextArgumentOfOption: # If this argument wasn't a value of an argument (like a filepath)
            if value == '-f' and not nextArgumentOfOption: # The user has introduced a file path, we'll open the file and convert it's contents
                nextArgumentOfOption = True # We activate a block for the next argument, as it will be used for the file path
                filePath = arguments[index+1]
                file = open(filePath, 'r')
                for line in file:
                    # For each line, we'll attempt to convert and print the result
                    try:
                        value = decimalToHexadecimal(int(line))
                        print(value)
                    except ValueError:
                        print("Line with {} cannot be converted into hexadecimal".format(line.strip('\n')), file=sys.stderr)
            elif (value == '-n') or (len(arguments) - 1 == index): # The user has introduced a number directly or this is the last of arguments
                nextArgumentOfOption = True
                value = int(arguments[index + 1]) # We convert the value inputted and will print the conversion
                try:
                    value = decimalToHexadecimal(value)
                    print(value)
                except ValueError:
                    print("The value introduced cannot be converted", file=sys.stderr)
        else:
            # In this case, the argument passed is used in any of the previous options, so we'll ignore it and reset the blocker
            nextArgumentOfOption = True


if __name__ == '__main__':
    main()

