def decimalToHexadecimal(decimal):
    return hex(decimal).split('x')[-1]

